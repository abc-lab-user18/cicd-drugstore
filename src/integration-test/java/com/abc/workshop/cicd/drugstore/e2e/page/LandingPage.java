package com.abc.workshop.cicd.drugstore.e2e.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LandingPage {

    WebDriver driver;

    @FindBy(css = "[data-node-se='landing-page']")
    WebElement landingPage;

    @FindBy(css = "[data-node-se='product-1']")
    WebElement firstProduct;

    ProductSection firstProductSection;

    public LandingPage(WebDriver driver) {
        this.driver = driver;

        //This initElements method will create all WebElements
        PageFactory.initElements(driver, this);

        firstProductSection = new ProductSection(firstProduct);
    }

    public boolean isOnLandingPage() {
        return landingPage.isDisplayed() && firstProduct.isDisplayed();
    }

    public WebElement getLandingPage() {
        return landingPage;
    }

    public WebElement getFirstProduct() {
        return firstProduct;
    }

    public ProductSection getFirstProductSection() {
        return firstProductSection;
    }
}
