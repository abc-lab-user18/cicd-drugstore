package com.abc.workshop.cicd.drugstore.controllers;

import com.abc.workshop.cicd.drugstore.dto.PagedProductListDTO;
import com.abc.workshop.cicd.drugstore.service.ProductService;
import com.abc.workshop.cicd.drugstore.service.ShoppingCartService;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LandingPageController {

    @Value("${spring.profiles.active:default}")
    private String activeProfile;

    @Value("${spring.datasource.url}")
    private String datasourceURL;

    @Autowired
    private ProductService productService;

    @Autowired
    private ShoppingCartService shoppingCartService;

    @GetMapping("/")
    public ModelAndView homePage(
            @RequestParam(value = "page", required = false, defaultValue = "1") String pageStr,
            @RequestParam(value = "size", required = false, defaultValue = "10") String sizeStr,
            @RequestParam(value = "search", required = false, defaultValue = "") String searchString) {
        ModelAndView modelAndView = new ModelAndView("home");

        int page = NumberUtils.toInt(pageStr, 1);
        int size = NumberUtils.toInt(sizeStr, 10);

        if (page >= 1) {
            page--;
        }

        PagedProductListDTO productList = productService.getProducts(page, size);

        modelAndView.addObject("pager", productList);
        modelAndView.addObject("activeProfile", activeProfile);
        modelAndView.addObject("datasourceURL", datasourceURL);

        modelAndView.addObject("productMap", shoppingCartService.getProductsInCart());
        modelAndView.addObject("products", productList.getResults());
        return modelAndView;
    }

}
