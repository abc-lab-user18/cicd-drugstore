package com.abc.workshop.cicd.drugstore.dao.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.abc.common.exception.ResourceNotFoundException;
import com.abc.common.utils.MapperUtils;
import com.abc.workshop.cicd.drugstore.dao.CustomerDAO;
import com.abc.workshop.cicd.drugstore.data.jpa.entity.CustomerEntity;
import com.abc.workshop.cicd.drugstore.data.jpa.repository.CustomerRepository;
import com.abc.workshop.cicd.drugstore.model.Customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerDAOImpl implements CustomerDAO {
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private MapperUtils mapper;

    public List<Customer> getAllCustomers() {
        List<CustomerEntity> customers = customerRepository.findAll();

        return customers.stream().map(customer -> mapper.map(customer, Customer.class)).collect(Collectors.toList());
    }

    public Customer getCustomer(Long customerId) {
        Optional<CustomerEntity> optionalCustomerEntity = customerRepository.findById(customerId);

        return optionalCustomerEntity.map(customerEntity -> mapper.map(customerEntity, Customer.class))
                .orElseThrow(() -> new ResourceNotFoundException("Customer not found: " + customerId));
    }

    public Customer saveCustomer(Customer customer) {
        CustomerEntity customerEntity = mapper.map(customer, CustomerEntity.class);

        return mapper.map(customerRepository.save(customerEntity), Customer.class);
    }

    public void deleteCustomer(Long customerId) {
        customerRepository.deleteById(customerId);
    }
}
