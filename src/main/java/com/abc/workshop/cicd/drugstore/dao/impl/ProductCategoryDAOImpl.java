package com.abc.workshop.cicd.drugstore.dao.impl;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.abc.common.exception.ResourceNotFoundException;
import com.abc.common.utils.MapperUtils;
import com.abc.workshop.cicd.drugstore.dao.ProductCategoryDAO;
import com.abc.workshop.cicd.drugstore.data.jpa.entity.ProductCategoryEntity;
import com.abc.workshop.cicd.drugstore.data.jpa.repository.ProductCategoryRepository;
import com.abc.workshop.cicd.drugstore.model.Product;
import com.abc.workshop.cicd.drugstore.model.ProductCategory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ProductCategoryDAOImpl implements ProductCategoryDAO {
    @Autowired
    private ProductCategoryRepository productCategoryRepository;

    @Autowired
    private MapperUtils mapper;

    public List<ProductCategory> getProductCategories() {
        List<ProductCategoryEntity> productCategories = productCategoryRepository.findAll();

        return productCategories.stream().map(productCategory -> mapper.map(productCategory, ProductCategory.class)).collect(Collectors.toList());
    }

    public ProductCategory getProductCategory(Long productCategoryId) {
        Optional<ProductCategoryEntity> optionalProductCategoryEntity = productCategoryRepository.findById(productCategoryId);

        return optionalProductCategoryEntity.map(productCategoryEntity -> mapper.map(productCategoryEntity, ProductCategory.class))
                .orElseThrow(() -> new ResourceNotFoundException("ProductCategory not found: " + productCategoryId));
    }

    public ProductCategory saveProductCategory(ProductCategory productCategory) {
        ProductCategoryEntity productCategoryEntity = mapper.map(productCategory, ProductCategoryEntity.class);

        return mapper.map(productCategoryRepository.save(productCategoryEntity), ProductCategory.class);
    }

    public void deleteProductCategory(Long productCategoryId) {
        productCategoryRepository.deleteById(productCategoryId);
    }
}
