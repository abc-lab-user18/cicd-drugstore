package com.abc.workshop.cicd.drugstore.data.jpa.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.abc.workshop.cicd.drugstore.data.jpa.utils.LocalDateTimeAttributeConverter;

@Entity
@Table(name = "product_category")
public class ProductCategoryEntity implements Serializable {
    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long productCategoryId;
    private String name;
    @Column(name = "CREATED_DATE_TIME")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    private LocalDateTime createdDateTime;
    @Column(name = "CREATED_BY")
    private String createdBy;
    @Column(name = "MODIFIED_DATE_TIME")
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    private LocalDateTime modifiedDateTime;
    @Column(name = "MODIFIED_BY")
    private String modifiedBy;

    public Long getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(Long productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(LocalDateTime createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(LocalDateTime modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}
