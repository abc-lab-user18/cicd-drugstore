package com.abc.workshop.cicd.drugstore.dao;

import com.abc.workshop.cicd.drugstore.model.FilterGroup;
import com.abc.workshop.cicd.drugstore.model.PagedProductList;
import com.abc.workshop.cicd.drugstore.model.Product;

import org.springframework.data.domain.Pageable;

/**
 * Data Access Object for performing product-related operations.
 */
public interface ProductDAO {
    /**
     * Returns the list of all product objects.
     *
     * @param pageRequest Request object used for paging and sorting
     * @param filters Object containing query filters
     * @return the paged list of product objects
     */
    PagedProductList getProducts(Pageable pageRequest, FilterGroup filters);

    /**
     * Returns the product object for the specified product ID, if found.
     *
     * @param productId the product ID
     * @return the product object
     */
    Product getProduct(Long productId);

    /**
     * Saves product info.
     *
     * @param product the product information
     * @return the updated product object
     */
    Product saveProduct(Product product);

    /**
     * Deletes an existing product.
     *
     * @param productId the ID of the product to delete
     */
    void deleteProduct(Long productId);
}
