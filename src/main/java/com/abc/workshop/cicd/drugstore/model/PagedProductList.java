package com.abc.workshop.cicd.drugstore.model;

import java.util.List;

public class PagedProductList {
    List<Product> results;
    Long totalRecords;
    Integer totalPages;
    Integer pageNumber;

    public PagedProductList() {
        // Default Constructor
    }

    public PagedProductList(List<Product> results, Integer totalPages, Long totalRecords, Integer pageNumber) {
        this.results = results;
        this.totalPages = totalPages;
        this.totalRecords = totalRecords;
        this.pageNumber = pageNumber;
    }

    public List<Product> getResults() {
        return results;
    }

    public void setResults(List<Product> results) {
        this.results = results;
    }

    public Long getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }
}
