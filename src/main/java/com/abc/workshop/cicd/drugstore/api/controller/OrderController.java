package com.abc.workshop.cicd.drugstore.api.controller;

import java.util.List;

import javax.validation.Valid;

import com.abc.workshop.cicd.drugstore.dto.OrderDTO;
import com.abc.workshop.cicd.drugstore.service.OrderService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller used to create, retrieve, update, and delete orders.
 */
@RestController
@RequestMapping({"/api/v1/orders"})
public class OrderController {
    @Autowired
    private OrderService orderService;

    /**
     * Returns a list of all order resources for a given customer.
     *
     * @param customerId The customer ID
     * @return The list of orders
     */
    @RequestMapping(value = "/customer/{customerId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<OrderDTO>> getAllOrders(@PathVariable Long customerId) {
        return new ResponseEntity<List<OrderDTO>>(orderService.getAllOrders(customerId), HttpStatus.OK);
    }

    /**
     * Returns an order resource if found using the specified order id.
     *
     * @param orderId The order ID
     * @return The order resource
     */
    @RequestMapping(value = "/{orderId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderDTO> getOrder(@PathVariable Long orderId) {
        return new ResponseEntity<OrderDTO>(orderService.getOrder(orderId), HttpStatus.OK);
    }

    /**
     * Creates a new order resource.
     *
     * @return The create response
     */
    @RequestMapping(value = "", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderDTO> createOrder(@RequestBody @Valid OrderDTO request) {
        return new ResponseEntity<OrderDTO>(orderService.saveOrder(request), HttpStatus.CREATED);
    }

    /**
     * Updates an existing order resource.
     *
     * @return The update response
     */
    @RequestMapping(value = "/{orderId}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderDTO> updateOrder(@RequestBody @Valid OrderDTO request, @PathVariable Long orderId) {
        return new ResponseEntity<OrderDTO>(orderService.saveOrder(request), HttpStatus.OK);
    }

    /**
     * Deletes an existing order resource.
     *
     * @param orderId The order ID
     * @return The delete response
     */
    @RequestMapping(value = "/{orderId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<OrderDTO> deleteOrder(@PathVariable Long orderId) {
        orderService.deleteOrder(orderId);

        return new ResponseEntity(HttpStatus.OK);
    }
}
