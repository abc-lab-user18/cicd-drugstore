package com.abc.workshop.cicd.drugstore.dto;

import java.util.List;
import java.util.StringJoiner;

public class PagedProductListDTO {
    List<ProductDTO> results;
    Long totalRecords;
    Integer totalPages;
    Integer pageNumber;

    public PagedProductListDTO() {
        // Default Constructor
    }

    public PagedProductListDTO(List<ProductDTO> results, Integer totalPages, Long totalRecords, Integer pageNumber) {
        this.results = results;
        this.totalPages = totalPages;
        this.totalRecords = totalRecords;
        this.pageNumber = pageNumber;
    }

    public List<ProductDTO> getResults() {
        return results;
    }

    public void setResults(List<ProductDTO> results) {
        this.results = results;
    }

    public Long getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public boolean hasNext() {
        return getPageNumber() < getTotalPages();
    }

    public boolean hasPrevious() {
        return getPageNumber() > 0;
    }

    public boolean indexOutOfBounds() {
        return this.getPageNumber() < 0 || this.getPageNumber() > this.getTotalPages();
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", PagedProductListDTO.class.getSimpleName() + "[", "]").add("results=" + results).add("totalRecords=" + totalRecords)
                .add("totalPages=" + totalPages).add("pageNumber=" + pageNumber).toString();
    }
}
