package com.abc.workshop.cicd.drugstore.utils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;

import com.abc.workshop.cicd.drugstore.dto.AddressDTO;
import com.abc.workshop.cicd.drugstore.dto.CustomerDTO;
import com.abc.workshop.cicd.drugstore.dto.OrderDTO;
import com.abc.workshop.cicd.drugstore.dto.OrderDetailDTO;
import com.abc.workshop.cicd.drugstore.dto.ProductCategoryDTO;
import com.abc.workshop.cicd.drugstore.dto.ProductDTO;

public class TestDataHelper {

    public static ProductDTO getProduct(int index) {
        return new ProductDTO() {
            {
                setProductId(Long.valueOf("" + index));
                setName("Test product " + index);
                setDescription("Test description " + index);
                setPrice(new BigDecimal("" + (index * 11.11)));
                setCreatedDateTime(LocalDateTime.of(2018, Month.AUGUST, 24, index, 30, 40));
                setCreatedBy("Test create person " + index);
                setModifiedDateTime(LocalDateTime.of(2018, Month.AUGUST, 24, index, 30, 20));
                setModifiedBy("Test modify person " + index);
                setQuantityInStock(index);
            }
        };
    }

    public static ProductCategoryDTO getProductCategory(int index) {
        return new ProductCategoryDTO() {
            {
                setProductCategoryId(Long.valueOf("" + index));
                setName("Test product category " + index);
                setCreatedDateTime(LocalDateTime.of(2018, Month.AUGUST, 24, index, 30, 40));
                setCreatedBy("Test create person " + index);
                setModifiedDateTime(LocalDateTime.of(2018, Month.AUGUST, 24, index, 30, 20));
                setModifiedBy("Test modify person " + index);
            }
        };
    }

    public static OrderDTO getOrder(int index) {
        return new OrderDTO() {
            {
                setOrderId(Long.valueOf("" + index));
                setOrderDate(LocalDateTime.of(2018, Month.AUGUST, 24, index, 30, 40));
                setShipDate(LocalDateTime.of(2018, Month.AUGUST, 24, index, 30, 40));
                setLastModifiedDate(LocalDateTime.of(2018, Month.AUGUST, 24, index, 30, 40));
                setCustomer(TestDataHelper.getCustomer(index));
                setOrderDetails(new ArrayList<OrderDetailDTO>() {
                    {
                        TestDataHelper.getOrderDetail(index);
                    }
                });
                setCreatedDateTime(LocalDateTime.of(2018, Month.AUGUST, 24, index, 30, 40));
                setCreatedBy("Test create person " + index);
                setModifiedDateTime(LocalDateTime.of(2018, Month.AUGUST, 24, index, 30, 20));
                setModifiedBy("Test modify person " + index);
                setCustomer(TestDataHelper.getCustomer(index));
            }
        };
    }

    public static OrderDetailDTO getOrderDetail(int index) {
        return new OrderDetailDTO() {
            {
                setOrderDetailId(Long.valueOf("" + index));
                setQuantity(index);
                setPrice(new BigDecimal("" + (index * 11.11)));
                setProduct(TestDataHelper.getProduct(index));
                setCreatedDateTime(LocalDateTime.of(2018, Month.AUGUST, 24, index, 30, 40));
                setCreatedBy("Test create person " + index);
                setModifiedDateTime(LocalDateTime.of(2018, Month.AUGUST, 24, index, 30, 20));
                setModifiedBy("Test modify person " + index);
            }
        };
    }

    public static CustomerDTO getCustomer(int index) {
        return new CustomerDTO() {
            {
                setCustomerId(Long.valueOf("" + index));
                setFirstName("Test first name " + index);
                setLastName("Test last name " + index);
                setEmail("test" + index + "@test.com");
                setPhoneNumber("" + index + index + index + "-" + index + index + index + "-" + index + index + index + index);
                setAddresses(new ArrayList<AddressDTO>() {
                    {
                        TestDataHelper.getAddress(index);
                    }
                });
            }
        };
    }

    public static AddressDTO getAddress(int index) {
        return new AddressDTO() {
            {
                setId(Long.valueOf("" + index));
                setAddressType(1);
                setAddressLine1("Test address first line " + index);
                setAddressLine2("Test address second line " + index);
                setCity("Test city " + index);
                setState("TX");
                setPostalCode("75001");
                setCustomerId(Long.valueOf("" + index));
            }
        };
    }

}
