#!/usr/bin/env bash

APPSERVERURL="http://168.61.53.251"
SELENIUMHUBURL="http://104.45.158.13:4444/wd/hub"

COUNTER=0
ACTIVATOR_URL="${APPSERVERURL}/actuator/health"

while true
do
  STATUS=$(curl --max-time 10 -s -o /dev/null -w '%{http_code}' ${ACTIVATOR_URL})
  if [ $STATUS -eq 200  ]; then
    echo "Request to $ACTIVATOR_URL Got 200! Moving on"
    break
  elif [ $COUNTER -ge 10 ]; then
    echo "Unable to connect to $ACTIVATOR_URL    Failing build"
    exit 1
  else
    COUNTER=$((COUNTER+1))
    echo "Request to $ACTIVATOR_URL Got $STATUS :( Not done yet... Try $COUNTER of 10"
  fi
  sleep 10
done

echo $ACTIVATORURL
echo ./gradlew integrationTest \
    -Ddrugstore.url="$APPSERVERURL" \
    -Ddrugstore.useSeleniumHub=true \
    -Ddrugstore.seleniumHubUrl="$SELENIUMHUBURL" \
    --no-daemon \
    --stacktrace
